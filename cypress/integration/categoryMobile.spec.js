import home from '../support/pages/home'
import menuHeader from '../support/components/header'
import category from '../support/pages/category'
import global from '../support/pages/GlobalFunctions'


describe('Mobile category', () => {

    global.IgnoreJSErros()

    const departments = {
        celulares_e_smartphones: 0,
        eletrodomesticos: 1,
        tvs_e_videos: 2,
        informatica: 3,
        pneus: 4,
        eletroportateis: 5,
        esporte_e_lazer: 6,
        cama_mesa_e_banho: 7,
        audio: 8,
        ar_condicionado_e_ventilacao: 9,
        todos_os_departamentos: 10
    }

    context('CN04 Validar redirecionamento para a página de categoria de produto [Celulares e Smartphones]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Celulares e Smartphones' },
            { index: 1, text: 'Smartphones' },
            { index: 2, text: 'Celulares' }
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.celulares_e_smartphones)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN05 Validar redirecionamento para a página de categoria de produto [Eletrodomésticos]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Eletrodoméstico' },
            { index: 1, text: 'Geladeira' },
            { index: 2, text: 'Fogões' },
            { index: 3, text: 'Micro-ondas' },
            { index: 4, text: 'Coifas e Depuradores' },
            { index: 5, text: 'Máquina de Lavar' },
            { index: 6, text: 'Cooktop' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.eletrodomesticos)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN06 Validar redirecionamento para a página de categoria de produto [TVs e vídeo]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Tvs e Video' },
            { index: 1, text: 'Tvs' },
            { index: 2, text: 'Blu-Ray Player' },
            { index: 3, text: 'Home Theater' },
            { index: 4, text: 'Receiver' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.tvs_e_videos)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN07 Validar redirecionamento para a página de categoria de produto [Informática]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Informática' },
            { index: 1, text: 'Notebook' },
            { index: 2, text: 'Impressora' },
            { index: 3, text: 'Computador' },
            { index: 4, text: 'Tablet e iPad' },
            { index: 5, text: 'Acessórios' },
            { index: 6, text: 'Mundo Informática' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.informatica)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN08 Validar redirecionamento para a página de categoria de produto [Pneus]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Pneus' },
            { index: 1, text: 'Caminhões e Ônibus' },
            { index: 2, text: 'Carros' },
            { index: 3, text: 'Motos' },
            { index: 4, text: 'Tratores' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.pneus)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN09 Validar redirecionamento para a página de categoria de produto [Eletroportáteis]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Eletroportáteis' },
            { index: 1, text: 'Bebedouro e Purificador de Água' },
            { index: 2, text: 'Robô e Aspirador de Pó' },
            { index: 3, text: 'Fritadeira Elétrica' },
            { index: 4, text: 'Máquina de Costura' },
            { index: 5, text: 'Cafeteira' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.eletroportateis)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN10 Validar redirecionamento para a página de categoria de produto [Esportes e Lazer]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Esporte e Lazer' },
            { index: 1, text: 'Bicicleta' },
            { index: 2, text: 'Bicicleta 26' },
            { index: 3, text: 'Bicicleta 29' },
            { index: 4, text: 'Fitness' },
            { index: 5, text: 'Lazer e Jogos' },
            { index: 6, text: 'Esportes' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.esporte_e_lazer)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN11 Validar redirecionamento para a página de categoria de produto [Cama, Mesa e Banho]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Cama, Mesa e Banho' },
            { index: 1, text: 'Cama' },
            { index: 2, text: 'Mesa' },
            { index: 3, text: 'Banho' },
            { index: 4, text: 'Sousplat' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.cama_mesa_e_banho)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN12 Validar redirecionamento para a página de categoria de produto [Áudio]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Áudio' },
            { index: 1, text: 'Som Ambiente' },
            { index: 2, text: 'Áudio Profissional' },
            { index: 3, text: 'Caixa de Som' },
            { index: 4, text: 'MP3, MP4  e MP5 Players' },
            { index: 5, text: 'Vitrolas e Gramofones' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.audio)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN13 Validar redirecionamento para a página de categoria de produto [Ar Condicionado e Ventilação]', function () {
        const subcategories = [
            { index: 0, text: 'Ver tudo de Ventilação' },
            { index: 1, text: 'Acessórios de climatização' },
            { index: 2, text: 'Aquecedores ambientes' },
            { index: 3, text: 'Ar-condicionado' },
            { index: 4, text: 'Climatizador' },
            { index: 5, text: 'Cortina de Ar' },
            { index: 6, text: 'Ventilador' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.ar_condicionado_e_ventilacao)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })

    context('CN14 Validar redirecionamento para a página de categoria de produto [Todos os Departamentos]', function () {
        const subcategories = [
            { index: 0, text: 'Acessórios de Tecnologia' },
            { index: 1, text: 'Ar Condicionado e Ventilação' },
            { index: 2, text: 'Áudio' },
            { index: 3, text: 'Automotivo' },
            { index: 4, text: 'Bebês' },
            { index: 5, text: 'Bebidas' },
            { index: 6, text: 'Brinquedos' },
            { index: 7, text: 'Calçados' },
            { index: 8, text: 'Cama, Mesa e Banho' },
            { index: 9, text: 'Câmera, Drones e Filmadoras' },
            { index: 10, text: 'Casa, Construção e Ferramentas' },
            { index: 11, text: 'Celulares, Smartphones e Smartwatches' },
            { index: 12, text: 'Console e Jogos' },
            { index: 13, text: 'Decoração' },
            { index: 14, text: 'Descartáveis e Utilidades Gerais' },
            // { index: 15, text: 'Disney' }, Não segue o padrão da PLP
            // { index: 16, text: 'Disney Baby' }, Não segue o padrão da PLP
            { index: 17, text: 'Eletrodomésticos' },
            { index: 18, text: 'Eletroportáteis' },
            { index: 19, text: 'Esporte e Lazer' },
            { index: 20, text: 'Filmes, Músicas e Séries' },
            { index: 21, text: 'Fraldas' },
            { index: 22, text: 'Higiene e Perfumaria' },
            // { index: 23, text: 'Importados' }, Não tem filtros
            { index: 24, text: 'Informática' },
            { index: 25, text: 'Jardim e Varanda' },
            { index: 26, text: 'Limpeza e Lavanderia' },
            { index: 27, text: 'Linha Industrial' },
            { index: 28, text: 'Livros' },
            { index: 29, text: 'Malas e Mochilas' },
            // { index: 30, text: 'Marvel' }, Não tem título
            { index: 31, text: 'Moda e Acessórios' },
            { index: 32, text: 'Móveis' },
            // { index: 33, text: 'Mundo Gamer' }, Não tem filtro
            { index: 34, text: 'Papelaria' },
            { index: 35, text: 'Pet shop' },
            { index: 36, text: 'Pneus' },
            { index: 37, text: 'Relógios' },
            { index: 38, text: 'Rodas Veiculares' },
            { index: 39, text: 'Saúde e Beleza' },
            // { index: 40, text: 'Serviços' }, Página diferente de PLP
            // { index: 41, text: 'Star Wars' }, Não tem título
            { index: 42, text: 'Suplementos Alimentares' },
            { index: 43, text: 'Tabacaria' },
            { index: 44, text: 'Telefonia Fixa' },
            { index: 45, text: 'TVs e vídeo' },
            { index: 46, text: 'Utilidades Domésticas' },
        ]

        beforeEach(function () {
            home.goMobile()
            home.closeCookies()
            menuHeader.openMenuMobile()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndexMobile(departments.todos_os_departamentos)
                menuHeader.selectSubcategoryByIndexMobile(subcategory.index)

                category.verifyFilterMobile()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderByMobile()
            })
        })
    })
})
