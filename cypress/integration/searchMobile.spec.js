import home from '../support/pages/home'
import global from '../support/pages/GlobalFunctions'
import header from '../support/components/header'
import category from '../support/pages/category'
import searchFactory from '../factories/SearchFactory'


describe('Search', () => {
    global.IgnoreJSErros()

    it('CN18 Validar busca de produto válido', function () {
        const search = searchFactory.search()

        home.goMobile()
        home.closeCookies()
        header.searchMobile(search.validSearch)

        category.verifyFilterMobile()
        category.verifyShelve()
        category.verifyH1(search.validSearch)
        category.verifyOrderByMobile()
    })

    it('CN19 Validar busca de produto inválido', function () {
        const search = searchFactory.search()

        home.goMobile()
        home.closeCookies()
        header.searchMobile(search.invalidSearch)

        category.verifySearchNotFound()
    })

    it('CN20 Validar filtro da página de busca [Faixa de preço]', () => {
        const search = searchFactory.search()

        home.goMobile()
        home.closeCookies()
        header.searchMobile(search.validSearch)

        category.openFilter()
        category.openFilterSlider()
        category.selectSliderMobile()
        category.verifyValueSlider()
        category.applyFilter()
        category.verifyFilterMobile()
        category.verifyShelve()
        category.verifyH1(search.validSearch)
        category.verifyOrderByMobile()
    })
})