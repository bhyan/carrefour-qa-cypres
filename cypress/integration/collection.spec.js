import global from '../support/pages/GlobalFunctions'
import home from '../support/pages/home'
import category from '../support/pages/category'


describe('Collection', () => {

    global.IgnoreJSErros()

    it('CN22 Validar redirecionamento para a página da coleção', () => {
        home.go()
        home.scroll(1000)

        home.closeModalCep()
        home.closeCookies()
        home.getRandomCollection()

        category.verifyFilter()
        category.verifyShelve()
        category.verifyOrderBy()
    })
})