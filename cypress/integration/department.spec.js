import home from '../support/pages/home'
import header from '../support/components/header'
import category from '../support/pages/category'
import global from '../support/pages/GlobalFunctions'


describe('Department page', () => {
    global.IgnoreJSErros()

    context('CN03 Validar redirecionamento para a página de departamento', function () {
        const departments = [
            { index: 0, text: 'Celulares e Smartphones' },
            { index: 1, text: 'Eletrodomésticos' },
            { index: 2, text: 'Tvs e Vídeos' },
            { index: 3, text: 'Informática' },
            { index: 4, text: 'Pneus' },
            { index: 5, text: 'Eletroportáteis' },
            { index: 6, text: 'Esporte e Lazer' },
            { index: 7, text: 'Cama, Mesa e Banho' },
            { index: 8, text: 'Áudio' },
            { index: 9, text: 'Ar Condicionado e Ventilação' }
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            header.openMenuAllDepartments()
        })

        departments.forEach(function (department) {
            it(`${department.text}`, function () {
                header.selectCategory(department.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })
})

describe('Filter department', () => {
    global.IgnoreJSErros()

    it('CN16 Validar filtro de marca', () => {
        home.go()
        home.closeCookies()
        header.openMenuAllDepartments()
        header.selectRandomCategory()

        category.selectRandomBrand()
        category.verifyQuantityFilterSelected(1)
    })

    it('CN17 Validar filtro com mais de uma marca', () => {
        home.go()
        home.closeCookies()
        header.openMenuAllDepartments()
        header.selectRandomCategory()

        category.selectRandomBrand()
        category.verifyQuantityFilterSelected(1)

        category.selectRandomBrand()
        category.verifyQuantityFilterSelected(2)
    })
})