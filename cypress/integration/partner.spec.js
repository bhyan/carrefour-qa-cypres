import global from '../support/pages/GlobalFunctions'
import home from '../support/pages/home'
import category from '../support/pages/category'
import product from '../support/pages/product'
import checkout from '../support/pages/checkout'


describe('Partner', () => {

    global.IgnoreJSErros()

    it('CN23 Validar redirecionamento para a página de seller', () => {
        home.go('/parceiros')
        home.closeCookies()

        category.getRandomProduct()
        category.selectPartner()
        category.verifyFilter()
        category.verifyShelve()
        category.verifyH1('parceiros')
        category.verifyOrderBy()
    })

    it.skip('CN24 Validar adição de produtos de diferentes vendedores no carrinho', () => {
        home.go('/parceiros')
        home.closeCookies()

        category.verifyFilter()
        category.verifyShelve()
        category.verifyH1('parceiros')
        category.verifyOrderBy()
        category.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.verifyProductCart()

        home.go('/parceiros?page=2')

        category.verifyFilter()
        category.verifyShelve()
        category.verifyH1('parceiros')
        category.verifyOrderBy()
        category.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.verifyProductCart()
    })
})