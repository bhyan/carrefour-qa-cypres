import global from '../support/pages/GlobalFunctions'
import home from '../support/pages/home'
import category from '../support/pages/category'
import product from '../support/pages/product'
import checkout from '../support/pages/checkout'


describe('Tablet partner', () => {

    global.IgnoreJSErros()

    it('CN23 Validar redirecionamento para a página de seller', () => {
        home.goTablet('/parceiros')
        home.closeCookies()

        category.getRandomProduct()
        category.selectPartner()
        category.verifyFilterMobile()
        category.verifyShelve()
        category.verifyH1('parceiros')
        category.verifyOrderByMobile()
    })

    it.skip('CN24 Validar adição de produtos de diferentes vendedores no carrinho', () => {
        home.goTablet('/parceiros')
        home.closeCookies()

        category.verifyFilterMobile()
        category.verifyShelve()
        category.verifyH1('parceiros')
        category.verifyOrderBy()
        category.getRandomProduct()

        product.buyProductMobile()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.verifyProductCart()

        home.goTablet('/parceiros?page=2')

        category.verifyFilterMobile()
        category.verifyShelve()
        category.verifyH1('parceiros')
        category.verifyOrderBy()
        category.getRandomProduct()

        product.buyProductMobile()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.verifyProductCart()
    })
})