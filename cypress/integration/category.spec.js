import home from '../support/pages/home'
import menuHeader from '../support/components/header'
import category from '../support/pages/category'
import global from '../support/pages/GlobalFunctions'


describe('Category', () => {

    global.IgnoreJSErros()

    const departments = {
        celulares_e_smartphones: 0,
        eletrodomesticos: 1,
        tvs_e_videos: 2,
        informatica: 3,
        pneus: 4,
        eletroportateis: 5,
        esporte_e_lazer: 6,
        cama_mesa_e_banho: 7,
        audio: 8,
        ar_condicionado_e_ventilacao: 9,
        todos_os_departamentos: 10
    }

    context('CN04 Validar redirecionamento para a página de categoria de produto [Celulares e Smartphones]', function () {
        const subcategories = [
            { index: 0, text: 'Smartphones' },
            { index: 1, text: 'Celulares' },
            { index: 2, text: 'Quantidade de Câmeras' },
            { index: 3, text: 'Memória Interna' },
            { index: 4, text: 'Memória Ram' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {

            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.celulares_e_smartphones)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN05 Validar redirecionamento para a página de categoria de produto [Eletrodomésticos]', function () {
        const subcategories = [
            { index: 0, text: 'Geladeira' },
            { index: 1, text: 'Máquina de Lavar' },
            { index: 2, text: 'Fogões' },
            { index: 3, text: 'Micro-ondas' },
            { index: 4, text: 'Coifas e Depuradores' },
            { index: 5, text: 'Cooktop' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.eletrodomesticos)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN06 Validar redirecionamento para a página de categoria de produto [TVs e vídeo]', function () {
        const subcategories = [
            { index: 0, text: 'Tvs' },
            { index: 1, text: 'Marcas' },
            { index: 2, text: 'Polegadas' },
            { index: 3, text: 'Videos' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.tvs_e_videos)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN07 Validar redirecionamento para a página de categoria de produto [Informática]', function () {
        const subcategories = [
            { index: 0, text: 'Notebook' },
            { index: 1, text: 'Impressora' },
            { index: 2, text: 'Computador' },
            { index: 3, text: 'Tablet e iPad' },
            { index: 4, text: 'Acessórios' },
            { index: 5, text: 'Mundo Informática' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.informatica)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN08 Validar redirecionamento para a página de categoria de produto [Pneus]', function () {
        const subcategories = [
            { index: 0, text: 'Categorias' },
            { index: 1, text: 'Carros' },
            { index: 2, text: 'Marcas' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.pneus)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN10 Validar redirecionamento para a página de categoria de produto [Esportes e Lazer]', function () {
        const subcategories = [
            { index: 0, text: 'Bicicleta' },
            { index: 1, text: 'Fitness' },
            { index: 2, text: 'Lazer e Jogos' },
            { index: 3, text: 'Esportes' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.esporte_e_lazer)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN09 Validar redirecionamento para a página de categoria de produto [Eletroportáteis]', function () {
        const subcategories = [
            { index: 0, text: 'Para Cozinhar' },
            { index: 1, text: 'Para Limpar e Costurar' },
            { index: 2, text: 'Marcas' },
            { index: 3, text: 'Para Facilitar' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.eletroportateis)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN11 Validar redirecionamento para a página de categoria de produto [Cama, Mesa e Banho]', function () {
        const subcategories = [
            { index: 0, text: 'Cama' },
            { index: 1, text: 'Mesa' },
            { index: 2, text: 'Banho' },
            { index: 3, text: 'Marcas' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.cama_mesa_e_banho)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN12 Validar redirecionamento para a página de categoria de produto [Áudio]', function () {
        const subcategories = [
            { index: 0, text: 'Som Ambiente' },
            { index: 1, text: 'Principais Marcas' },
            { index: 2, text: 'Áudio Profissional' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.audio)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN13 Validar redirecionamento para a página de categoria de produto [Ar Condicionado e Ventilação]', function () {
        const subcategories = [
            { index: 0, text: 'Ar Condicionado' },
            { index: 1, text: 'Ventiladores' },
            { index: 2, text: 'Aquecedores' },
            { index: 3, text: 'Climatizador' },
            { index: 4, text: 'Cortina de Ar' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.ar_condicionado_e_ventilacao)
                menuHeader.selectCategory(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })

    context('CN14 Validar redirecionamento para a página de categoria de produto [Todos os Departamentos]', function () {
        const subcategories = [
            { index: 0, text: 'Acessórios de Tecnologia' },
            { index: 1, text: 'Ar Condicionado e Ventilação' },
            { index: 2, text: 'Áudio' },
            { index: 3, text: 'Automotivo' },
            { index: 4, text: 'Bebês' },
            { index: 5, text: 'Bebidas' },
            { index: 6, text: 'Brinquedos' },
            { index: 7, text: 'Calçados' },
            { index: 9, text: 'Eletrodomésticos' },
            { index: 10, text: 'Eletroportáteis' },
            { index: 11, text: 'Esporte e Lazer' },
            { index: 12, text: 'Filmes, Músicas e Séries' },
            { index: 13, text: 'Fraldas' },
            { index: 14, text: 'Higiene e Perfumaria' },
            { index: 16, text: 'Móveis' },
            { index: 18, text: 'Papelaria' },
            { index: 19, text: 'Petshop' },
            { index: 20, text: 'Pneus' },
            { index: 21, text: 'Relógios' },
            { index: 22, text: 'Rodas Veiculares' },
            { index: 23, text: 'Saúde e Beleza' },
            { index: 24, text: 'Cama, Mesa e Banho' },
            { index: 25, text: 'Câmera, Drones e Filmadoras' },
            { index: 26, text: 'Casa, Construção e Ferramentas' },
            { index: 27, text: 'Celulares e Smartphones' },
            { index: 28, text: 'Console e Jogos' },
            { index: 29, text: 'Decoração' },
            { index: 30, text: 'Descartáveis e Utilidades Gerais' },
            { index: 32, text: 'Informática' },
            { index: 33, text: 'Jardim e Varanda' },
            { index: 34, text: 'Limpeza e Lavanderia' },
            { index: 35, text: 'Linha Industrial' },
            { index: 36, text: 'Livros' },
            { index: 37, text: 'Malas e Mochilas' },
            { index: 39, text: 'Moda e Acessórios' },
            { index: 42, text: 'Suplementos Alimentares' },
            { index: 43, text: 'Tabacaria' },
            { index: 44, text: 'Telefonia Fixa' },
            { index: 45, text: 'TVs e vídeo' },
            { index: 46, text: 'Utilidades Domésticas' },
        ]

        beforeEach(function () {
            home.go()
            home.closeCookies()
            menuHeader.openMenuAllDepartments()
        })

        subcategories.forEach(function (subcategory) {
            it(`${subcategory.text}`, function () {
                menuHeader.openCategoryByIndex(departments.todos_os_departamentos)
                menuHeader.selectSubcategoryAllDepartments(subcategory.index)

                category.verifyFilter()
                category.verifyShelve()
                category.verifyH1()
                category.verifyOrderBy()
            })
        })
    })
})
