import home from '../support/pages/home'
import global from '../support/pages/GlobalFunctions'
import header from '../support/components/header'
import category from '../support/pages/category'
import searchFactory from '../factories/SearchFactory'


describe('Search', () => {
    global.IgnoreJSErros()

    it('CN18 Validar busca de produto válido', function () {
        const search = searchFactory.search()

        home.go()
        home.closeCookies()
        header.search(search.validSearch)

        category.verifyFilter()
        category.verifyShelve()
        category.verifyH1(search.validSearch)
    })

    it('CN19 Validar busca de produto inválido', function () {
        const search = searchFactory.search()

        home.go()
        home.closeCookies()
        header.search(search.invalidSearch)

        category.verifySearchNotFound()
    })

    it('CN20 Validar filtro da página de busca [Faixa de preço]', () => {
        const search = searchFactory.search()

        home.go()
        home.closeCookies()
        header.search(search.validSearch)

        category.verifyH1(search.validSearch)
        category.selectSlider()
        category.verifyValueSlider()
        category.verifyFilter()
        category.verifyShelve()
    })
})