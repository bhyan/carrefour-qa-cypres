import global from '../support/pages/GlobalFunctions'
import checkout from '../support/pages/checkout'
import home from '../support/pages/home'
import product from '../support/pages/product'
import signupFactory from '../factories/SignupFactory'


describe('Produto', () => {

    global.IgnoreJSErros()

    it.skip('CN36 Compra de um produto aleatório na home', function () {
        home.go()
        home.scroll(1000)

        home.closeCookies()
        home.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.finalizePurchase()

        const user = signupFactory.user()

        checkout.emailInput(user)
        checkout.firstNameInput(user)
        checkout.lastNameInput(user)
        checkout.cpfInput(user)
        checkout.birthDate(user)
        checkout.phoneInput(user)
        checkout.acceptedTerms(user)

        checkout.btnGoToShipping()

        checkout.cepInput(user)
        checkout.shippingInputNumber(user)
        checkout.btnGoToPayment()
    })

    it('CN02 Validar redirecionamento para a página de produto', () => {
        home.go()
        home.scroll(1000)

        home.closeCookies()
        home.getRandomProduct()

        product.verifyTitle()
        product.verifyPrice()
        product.verifyBuyButton()
        product.verifyProductDescription()
    })

    it.skip('CN26 Validar adição de um produto no carrinho', () => {
        home.go()
        home.scroll(1000)

        home.closeCookies()
        home.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()

        checkout.verifyProductCart()
    })

    it.skip('CN27 Validar adição de mais de um produto no carrinho', () => {
        home.go()
        home.scroll(1000)

        home.closeCookies()
        home.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.verifyProductCart()

        home.go()
        home.scroll(1000)

        home.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.verifyProductCart()
    })

    it.skip('CN31 Validar remoção de um produto do carrinho', () => {
        home.go()
        home.scroll(1000)

        home.closeCookies()
        home.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.verifyProductCart()

        checkout.removeProduct()
        checkout.verifyEmptyCart()
    })

    it.skip('CN33 Validar aumento na quantidade de produtos no carrinho', () => {
        home.go()
        home.scroll(1000)

        home.closeCookies()
        home.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.incrementItem()
        checkout.verifyQuantityItem(2)
    })

    it.skip('CN34 Validar diminuição na quantidade de produtos no carrinho', () => {
        home.go()
        home.scroll(1000)

        home.closeCookies()
        home.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.verifyQuantityItem(1)

        checkout.incrementItem()
        checkout.verifyQuantityItem(2)

        checkout.decrementItem()
        checkout.verifyQuantityItem(1)
    })

    it.skip('CN35 Validar limite de quantidade de mesmo item no carrinho', () => {
        home.go()
        home.scroll(1000)

        home.closeCookies()
        home.getRandomProduct()

        product.buyProduct()
        product.confirmProductChose()

        checkout.chooseExtendWarranty()
        checkout.incrementItem()
        checkout.verifyQuantityItem(5)
    })
})