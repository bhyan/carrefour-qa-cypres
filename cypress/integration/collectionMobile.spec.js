import global from '../support/pages/GlobalFunctions'
import home from '../support/pages/home'
import category from '../support/pages/category'


describe('Mobile collection', () => {

    global.IgnoreJSErros()

    it('CN22 Validar redirecionamento para a página da coleção', () => {
        home.goMobile()
        home.scroll(1000)

        home.closeCookies()
        home.getRandomCollection()

        category.verifyFilterMobile()
        category.verifyShelve()
        category.verifyOrderByMobile()
    })
})