import global from '../support/pages/GlobalFunctions'
import home from '../support/pages/home'


describe('Mobile home', () => {
    global.IgnoreJSErros()

    it('CN01 Validar página da home', function () {
        home.goMobile()
        home.closeCookies()
        home.scroll(3000)
        home.verifyShelve()
        home.scroll(3000)
        home.verifyBannersDepartments()
        home.scroll(3000)
        home.verifyBestOffer()
        home.scroll(3000)
        home.verifyMosaic()
        home.scroll(3000)
        home.verifyFavoriteBrands()
    })
})