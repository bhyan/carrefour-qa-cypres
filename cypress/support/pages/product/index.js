class ProductPage {
    buyProduct() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })
        cy.wait(20000)
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.vtex-flex-layout-0-x-flexColChild--services-suggestions-pdp > .vtex-flex-layout-0-x-flexCol > .vtex-flex-layout-0-x-flexColChild > :nth-child(1) > .carrefourbr-carrefour-components-0-x-buyButtonContainer button .vtex-button__label')
            .click({ scrollBehavior: false, force: true })
    }

    buyProductMobile() {
        cy.wait(10000)
        cy.scrollTo(0, 100)
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.carrefourbr-carrefour-components-0-x-buyButtonContainer > button', { timeout: 50000 }).eq(1).click({ scrollBehavior: false })
    }

    buyProductTablet() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })
        cy.scrollTo(0, 100)

        cy.get('.carrefourbr-carrefour-components-0-x-buyButtonContainer w-100 > button', { timeout: 50000 }).eq(0).click({ scrollBehavior: false })
    }

    confirmProductChose() {
        cy.wait(5000)

        cy.get('body').then($body => {
            if ($body.find('.carrefourbr-carrefour-components-0-x-modalBuyButton button').length) {
                cy.get('.carrefourbr-carrefour-components-0-x-modalBuyButton button', { timeout: 30000 }).click({ scrollBehavior: false })
            }
        })
    }

    verifyTitle() {
        cy.get('.vtex-store-components-3-x-productBrand').should('be.visible')
    }

    verifyPrice() {
        cy.get('.vtex-flex-layout-0-x-flexCol--price-conditions-pdp > div > div > div > div > div > div > div > span > div > span > .carrefourbr-carrefour-components-0-x-currencySellingPrice').should('be.visible')
    }

    verifyShelve() {
        cy.scrollTo(0, 1000)
        cy.get('.vtex-product-summary-2-x-element').eq(0).should('be.visible')
    }

    verifyHeaderBuy() {
        cy.get('.vtex-flex-layout-0-x-flexRow--sticky-buy-button').should('be.visible')
    }

    verifyBuyButton() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.carrefourbr-carrefour-components-0-x-buyButtonContainer button', { timeout: 50000 }).eq(1)
            .should('be.visible')
    }

    verifyProductDescription() {
        cy.scrollTo('bottom')
        cy.get('.vtex-store-components-3-x-productDescriptionTitle').contains('Descrição do Produto')
    }

    closeCookies() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('body').then($body => {
            if ($body.find('button#onetrust-accept-btn-handler').length) {
                cy.get('button#onetrust-accept-btn-handler', { timeout: 30000 }).click()
            }
        })
    }

    verifyTitleMobile() {
        cy.get('.vtex-store-components-3-x-productNameContainer').should('be.visible')
    }

    verifyPriceMobile() {
        cy.get('.carrefourbr-carrefour-components-0-x-discountPrice').should('be.visible')
    }
}

export default new ProductPage