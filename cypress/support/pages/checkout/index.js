class CheckoutPage {
    chooseExtendWarranty() {
        cy.wait(20000)
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('body').then($body => {
            if ($body.find('.carrefourbr-carrefour-components-0-x-addWarrantyToCart > button').length) {

                cy.waitUntil(() => cy.get('.vtex-radio__label'))
                    .then(($label) => {
                        const items = $label.toArray()
                        return Cypress._.sample(items)
                    })
                    .then(($label) => {
                        expect(Cypress.dom.isJquery($label), 'jQuery element').to.be.true
                    }).click({ force: true })

                cy.get('.carrefourbr-carrefour-components-0-x-addWarrantyToCart > button', { timeout: 30000 }).click({ scrollBehavior: false })
            }
        })
    }

    verifyProductCart() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.product-item')
            .should('be.visible')
    }

    removeProduct() {
        cy.get('.item-link-remove').click()
    }

    verifyEmptyCart() {
        cy.get('.empty-cart-title')
            .should('be.visible')
            .should('have.text', 'Seu carrinho está vazio. :(')
    }

    finalizePurchase() {
        cy.wait(20000)

        cy.get('#cart-to-orderform', { timeout: 20000 }).click()
    }

    cepInput(user) {
        cy.get('#ship-postalCode')
            .clear()
            .type(user.address.cep)
    }

    btnGoToShipping() {
        cy.get('#go-to-shipping').click()
    }

    btnGoToPayment() {
        cy.get('button#btn-go-to-payment').click()
    }

    emailInput(user) {
        cy.get('#client-pre-email')
            .clear()
            .type(user.email)

        cy.get('#btn-client-pre-email')
            .click({ scrollBehavior: false })
    }

    firstNameInput(user) {
        cy.get('#client-first-name')
            .clear()
            .type(user.firstName)
    }

    lastNameInput(user) {
        cy.get('#client-last-name')
            .clear()
            .type(user.lastName)
    }

    cpfInput(user) {
        cy.get('#client-document')
            .clear()
            .type(user.cpf)
    }

    birthDate(user) {
        cy.get('#datepicker')
            .clear()
            .type(user.birth)
    }

    phoneInput(user) {
        cy.get('#client-phone')
            .clear()
            .type(user.phone)
    }

    acceptedTerms() {
        cy.get('#acceptedTerms')
            .check()
    }

    shippingInputNumber(user) {
        cy.get('#ship-number')
            .clear()
            .type(user.address.number)
    }

    checkPaymentMethodBoleto() {
        cy.get('.required.payment-group-list-btn a#payment-group-bankInvoicePaymentGroup')
    }

    checkPaymentMethodPix() {
        cy.get('.required.payment-group-list-btn a#payment-group-instantPaymentPaymentGroup')
    }

    incrementItem() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.item-quantity-change-increment').eq(0).click()
    }

    decrementItem() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.item-quantity-change-decrement').eq(0).click()
    }

    verifyQuantityItem(quantity) {
        cy.get('input[id^="item-quantity"]').eq(0).invoke('val').should('eq', quantity.toString());
    }
}

export default new CheckoutPage