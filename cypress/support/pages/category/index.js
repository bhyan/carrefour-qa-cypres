class CategoryPage {
    verifyFilter() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.vtex-flex-layout-0-x-flexCol--filterCol', { timeout: 20000 }).should('be.visible')
    }

    verifyShelve() {
        cy.get('.vtex-search-result-3-x-galleryItem', { timeout: 20000 }).eq(0).should('be.visible')
    }

    verifyH1() {
        cy.get('h1').should('be.visible')
    }

    verifyOrderBy() {
        cy.get('.vtex-flex-layout-0-x-flexCol--orderByMobileCol').should('be.visible')
    }

    verifySearchNotFound() {
        cy.get('.vtex-search-result-3-x-searchNotFound').should('be.visible')
    }

    selectSlider() {
        cy.get('.vtex-flex-layout-0-x-flexColChild > .vtex-search-result-3-x-filters--layout .vtex-search-result-3-x-filter__container--priceRange .vtex-slider__base')
            .click({ scrollBehavior: false })
    }

    selectSliderMobile() {
        cy.get('.ReactCollapse--collapse .vtex-search-result-3-x-filter__container--priceRange .vtex-slider__base-internal')
            .click()
    }

    openFilter() {
        cy.wait(1000)
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.vtex-search-result-3-x-filterPopupButton > .vtex-search-result-3-x-filterPopupTitle')
            .realHover({ scrollBehavior: false })
            .click({ scrollBehavior: false })
    }

    openFilterSlider() {
        cy.get(".vtex-search-result-3-x-filterAccordionItemBox--faixa-de-preco").realSwipe("toBottom");
        cy.get('.vtex-search-result-3-x-filterAccordionItemBox--faixa-de-preco').click({ scrollBehavior: false })
    }

    verifyValueSlider() {
        cy.get('.vtex-flex-layout-0-x-flexColChild > .vtex-search-result-3-x-filters--layout .vtex-slider__left-value')
            .should('not.eq', 'R$ 0,00')
    }

    applyFilter() {
        cy.get('.vtex-search-result-3-x-filterApplyButtonWrapper > .vtex-button').click()
    }

    selectBand() {
        cy.get('[style="height: auto;"] > .vtex-search-result-3-x-filterContent > .vtex-search-result-3-x-filterItem--nao-informado > .vtex-checkbox__line-container > .vtex-checkbox__label').click()
    }

    selectRandomBrand() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })
        cy.scrollTo(0, 1000)
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.waitUntil(() => cy.get('.vtex-flex-layout-0-x-flexCol--filterCol .vtex-search-result-3-x-filter__container--brand input:not(:checked)'))
            .then(($checkBox) => {
                const items = $checkBox.toArray()
                return Cypress._.sample(items)
            })
            .then(($checkBox) => {
                expect(Cypress.dom.isJquery($checkBox), 'jQuery element').to.be.true
            }).check({ force: true })
    }

    verifyQuantityFilterSelected(quantity) {
        cy.get('.pr0 > .vtex-search-result-3-x-filters--layout .vtex-search-result-3-x-filter__container--selectedFilters  .vtex-checkbox__line-container label')
            .should('have.length', quantity)
    }

    getRandomProduct() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.waitUntil(() => cy.get('.vtex-product-price-1-x-currencyContainer'))
            .then(($label) => {
                const items = $label.toArray()
                return Cypress._.sample(items)
            })
            .then(($label) => {
                expect(Cypress.dom.isJquery($label), 'jQuery element').to.be.true
            }).click({ force: true })
    }

    selectPartner() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })
        cy.scrollTo(0, 100)

        cy.get('a[href^="/parceiro/"]').click({ scrollBehavior: false })
    }

    verifyOrderByMobile() {
        cy.get('.vtex-search-result-3-x-orderByButton').should('be.visible')
    }

    verifyFilterMobile() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.vtex-search-result-3-x-filterPopupButton').should('be.visible')
    }
}

export default new CategoryPage