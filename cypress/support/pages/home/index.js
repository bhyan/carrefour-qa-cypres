class HomePage {
    goMobile(page = '/') {
        cy.viewport(390, 844)
        cy.visit(page)
    }

    goTablet(page = '/') {
        cy.viewport(820, 1180)
        cy.visit(page)
    }

    go(page = '/') {
        cy.visit(page)
    }

    closeModalCep() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('body').then($body => {
            if ($body.find('button[class=styles_closeButton__20ID4]').length) {
                cy.get('button[class=styles_closeButton__20ID4]').click()
            }
        })
    }

    closeCookies() {
        cy.setCookie('OptanonAlertBoxClosed', new Date().toString())
    }

    scroll(positionY) {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })
        cy.scrollTo(0, positionY)
    }

    getRandomProduct() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.waitUntil(() => cy.get('.vtex-product-summary-2-x-imageStackContainer'))
            .then(($label) => {
                const items = $label.toArray()
                return Cypress._.sample(items)
            })
            .then(($label) => {
                expect(Cypress.dom.isJquery($label), 'jQuery element').to.be.true
            }).click({ force: true })
    }

    getRandomCollection() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.waitUntil(() => cy.get('a[href^="/colecao"]').should('be.visible'))
            .then(($label) => {
                const items = $label.toArray()
                return Cypress._.sample(items)
            })
            .then(($label) => {
                expect(Cypress.dom.isJquery($label), 'jQuery element').to.be.true
            }).click({ force: true })
    }

    getProduct() {
        cy.get('.vtex-flex-layout-0-x-flexCol--chaordic-shelf-middle-content > :nth-child(1) > .vtex-store-components-3-x-container > .carrefourbr-carrefour-search-0-x-chaordicShelfContainer > :nth-child(2) > .carrefourbr-carrefour-search-0-x-sliderLayoutContainer > .carrefourbr-carrefour-search-0-x-sliderTrackContainer > [data-testid="slider-track"] > .carrefourbr-carrefour-search-0-x-slide--firstVisible.carrefourbr-carrefour-search-0-x-slide--visible > .carrefourbr-carrefour-search-0-x-slideChildrenContainer > .w-auto > .vtex-product-summary-2-x-container > .vtex-product-summary-2-x-clearLink > .vtex-product-summary-2-x-element > .vtex-product-summary-2-x-imageWrapper > .dib > .vtex-product-summary-2-x-imageNormal')
            .should('be.visible').click()
    }

    verifyShelve() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })
        cy.get('.vtex-product-summary-2-x-element', { timeout: 20000 }).eq(0).should('be.visible')
    }

    verifyBannersDepartments() {
        cy.get('.vtex-flex-layout-0-x-flexRowContent--bottons-departamento').should('be.visible')
    }

    verifyBestOffer() {
        cy.get('.vtex-slider-layout-0-x-sliderLayoutContainer--offers-festival-slider').should('be.visible')
    }

    verifyMosaic() {
        cy.get('.vtex-flex-layout-0-x-flexRowContent--mosaic-columns').should('be.visible')
    }

    verifyFavoriteBrands() {
        cy.get('.vtex-slider-layout-0-x-sliderTrackContainer--favorite-brands-slider').should('be.visible')
    }
}

export default new HomePage