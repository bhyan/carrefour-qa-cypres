class HeaderComponent {
    openMenuAllDepartments() {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })
        cy.get('li.vtex-menu-2-x-menuItem--header-desktop-menu > div > span > div').click({ scrollBehavior: false })
    }

    selectRandomCategory() {
        cy.waitUntil(() => cy.get('.vtex-menu-2-x-menuContainerNav--all-departments-menu > ul > li'))
            .then(($label) => {
                const items = $label.toArray()
                return Cypress._.sample(items)
            })
            .then(($label) => {
                expect(Cypress.dom.isJquery($label), 'jQuery element').to.be.true
            }).click({ scrollBehavior: false })
    }

    search(product) {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.vtex-styleguide-9-x-input').type(product)
        cy.get('button.vtex-store-components-3-x-searchBarIcon').click()
    }

    searchMobile(product) {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.carrefourbr-carrefour-search-0-x-searchBar .vtex-styleguide-9-x-input').type(product)
        cy.get('button[type=submit]').click()
    }

    openCategoryByIndex(index) {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.vtex-menu-2-x-menuContainerNav--all-departments-menu > ul > li').eq(index).realHover({ scrollBehavior: false })

        cy.get('.vtex-flex-layout-0-x-flexRow--shopping-desktop-drawer-submenu-img').should('be.visible')
    }


    selectCategory(index) {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-title').eq(index).click({ scrollBehavior: false })
    }

    // selectSubcategoryCellphone(index) {
    //     cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

    //     cy.get('.vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-title').eq(index).click({ scrollBehavior: false })
    // }

    // selectSubcategoryHomeAppliance(index) {
    //     cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

    //     cy.get('.vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-title').eq(index).click({ scrollBehavior: false })
    // }

    // selectSubcategoryTvsAndVideo(index) {
    //     cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

    //     cy.get('.vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-title').eq(index).click({ scrollBehavior: false })
    // }

    // selectSubcategoryComputing(index) {
    //     cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

    //     cy.get('.vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-title').eq(index).click({ scrollBehavior: false })
    // }

    // selectSubcategoryTires(index) {
    //     cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

    //     cy.get('.vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-title').eq(index).click({ scrollBehavior: false })
    // }

    // selectSubcategorySportsAndLeisure(index) {
    //     cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

    //     cy.get('.vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-title').eq(index).click({ scrollBehavior: false })
    // }

    // selectSubcategoryBedTableBath(index) {
    //     cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

    //     cy.get('.vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-title').eq(index).click({ scrollBehavior: false })
    // }

    // selectSubcategoryAudio(index) {
    //     cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

    //     cy.get('.vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-title').eq(index).click({ scrollBehavior: false })
    // }

    // selectSubcategoryAirConditioningAndVentilation(index) {
    //     cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

    //     cy.get('vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-items').eq(index).click({ scrollBehavior: false })
    // }

    selectSubcategoryAllDepartments(index) {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('a .vtex-menu-2-x-styledLinkContent--shopping-desktop-drawer-submenu-items').eq(index).click({ scrollBehavior: false })
    }

    openMenuMobile() {
        cy.get('.vtex-store-drawer-0-x-openIconContainer--mobile-drawer').click({ scrollBehavior: false })
    }

    openCategoryByIndexMobile(index) {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.vtex-menu-2-x-menuContainer--drawer-mobile-menu > .vtex-menu-2-x-menuItem--drawer-shopping-menu-mobile')
            .eq(index).click({ scrollBehavior: false })
    }

    selectSubcategoryByIndexMobile(index) {
        cy.waitForStableDOM({ pollInterval: 1000, timeout: 30000 })

        cy.get('.vtex-menu-2-x-menuItem--shopping-first-level-submenu-items')
            .eq(index).click()
    }

}

export default new HeaderComponent