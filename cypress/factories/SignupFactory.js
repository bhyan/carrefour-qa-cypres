const faker = require('faker')
const cpf = require('cpf')

faker.locale = 'pt_BR'

export default {

    user: function () {

        const firstName = faker.name.findName()

        const data = {
            firstName: firstName,
            lastName: faker.name.lastName(),
            cpf: cpf.generate(false, false),
            email: faker.internet.email(firstName),
            birth: '21111960',
            phone: faker.phone.phoneNumber('11########'),
            address: {
                cep: '04534011',
                number: faker.datatype.number()
            },
        }

        return data
    }
}